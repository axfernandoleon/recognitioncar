from __future__ import print_function, unicode_literals
from pprint import pprint
from PyInquirer import style_from_dict, Token, prompt, Separator
from progress.bar import IncrementalBar
from src.detect import detect
import os

if __name__ == '__main__':

    from pathlib import Path
    from json_environ import Environ

    env_path = os.path.join(Path('.'), '.env.json')
    env = Environ(path=env_path)
    input_array = env("INPUT", default=False)

    style = style_from_dict({
        Token.Separator: '#cc5454',
        Token.QuestionMark: '#673ab7 bold',
        Token.Selected: '#cc5454',  # default
        Token.Pointer: '#673ab7 bold',
        Token.Instruction: '',  # default
        Token.Answer: '#f44336 bold',
        Token.Question: '',
    })
    questions = [
        {
            'type': 'list',
            'message': 'Select',
            'name': 'toppings',
            'choices': input_array,
            'validate': lambda answer: 'You must choose at least one topping.'
            if len(answer) == 0 else True
        },
        {
            'type': 'confirm',
            'message': 'Guardo la salida de video?',
            'name': 'save_video',
            'default': True,
        },
    ]
    answers = prompt(questions, style=style)

    try:
        for i in IncrementalBar('Loading settings').iter(input_array):

            select = answers['toppings']
            position = [index for index, i in enumerate(
                input_array) if(i['name'] == select)]
            input_video = input_array[position[0]]

            # Settings variable input
            NAME = input_video["name"]
            URL = input_video["url"]
            DROI = eval(input_video["droi"])
            line_detect = eval(input_video["line_detect"])
        # Init recognition
        print("\033[96m[Starting up]\033[0m")
        detect(URL, DROI, line_detect,answers['save_video'])

    except KeyError as identifier:
        pass
