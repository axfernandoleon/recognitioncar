# OpenCV Python program to detect cars in video frame
# import libraries of python OpenCV
import cv2

# capture frames from a video
cap = cv2.VideoCapture(
    'rtsp://admin:Kradac01@190.57.168.163:555/Streaming/Channels/2')

# Trained XML classifiers describes some features of some object we want to detect
car_cascade = cv2.CascadeClassifier('cars.xml')

# loop runs if capturing has been initialized.
while True:
    # reads frames from a video
    ret, frames = cap.read()

    # convert to gray scale of each frames
    gray = cv2.cvtColor(frames, cv2.COLOR_BGR2GRAY)
    # Filtros
    grayFiltered = cv2.bilateralFilter(gray, 1, 50, 50)
    # grayFiltered = cv2.GaussianBlur(grayFiltered, (5, 5), 3)

    # Apply Histogram
    # grayFiltered = cv2.equalizeHist(grayFiltered)

    # mask = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernalOp)
    # mask2 = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernalOp)

    # Detects cars of different sizes in the input image
    cars = car_cascade.detectMultiScale(grayFiltered, 1.1, 1)

    # To draw a rectangle in each cars
    for (x, y, w, h) in cars:
        cv2.rectangle(frames, (x, y), (x+w, y+h), (0, 0, 255), 2)

        # Display frames in a window
        cv2.imshow('video2', frames)

        # Wait for Esc key to stop
        if cv2.waitKey(33) == 27:
            break

# De-allocate any associated memory usage
cv2.destroyAllWindows()
