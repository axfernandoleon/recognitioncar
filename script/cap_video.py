from __future__ import print_function, unicode_literals
from pprint import pprint
from PyInquirer import style_from_dict, Token, prompt, Separator
from progress.bar import IncrementalBar
import cv2
import os
from datetime import datetime

def save_video(url):
    # capture frames from a video
    cap = cv2.VideoCapture(url)
    # Generate name output
    uniq_filename = datetime.utcnow().strftime('%Y-%m-%d %H_%M_%S.%f')[:-3]
    # Save output recognition
    output = cv2.VideoWriter('../video/'+uniq_filename+'.avi',
                             cv2.VideoWriter_fourcc(*'XVID'), 20.0, (640, 480))
    # loop runs if capturing has been initialized.
    while cap.isOpened():
        # reads frames from a video
        ret, frame = cap.read()
        cv2.imshow('frame', frame)
        output.write(frame)
        # Wait for Esc key to stop
        if cv2.waitKey(1) & 0xff == ord('q'):
            break

    cap.release()
    output.release()
    cv2.destroyAllWindows()


if __name__ == '__main__':

    from pathlib import Path
    from json_environ import Environ

    env_path = os.path.join(Path('..'), '.env.json')
    env = Environ(path=env_path)
    input_array = env("INPUT", default=False)

    style = style_from_dict({
        Token.Separator: '#cc5454',
        Token.QuestionMark: '#673ab7 bold',
        Token.Selected: '#cc5454',  # default
        Token.Pointer: '#673ab7 bold',
        Token.Instruction: '',  # default
        Token.Answer: '#f44336 bold',
        Token.Question: '',
    })
    questions = [
        {
            'type': 'list',
            'message': 'Select',
            'name': 'toppings',
            'choices': input_array,
            'validate': lambda answer: 'You must choose at least one topping.'
            if len(answer) == 0 else True
        }
    ]
    answers = prompt(questions, style=style)
    try:
        for i in IncrementalBar('Loading settings').iter(input_array):

            select = answers['toppings']
            position = [index for index, i in enumerate(
                input_array) if(i['name'] == select)]
            input_video = input_array[position[0]]

            # Settings variable input
            NAME = input_video["name"]
            URL = input_video["url"]

        # Init recognition
        print("\033[96m[Starting up]\033[0m")
        save_video(URL)

    except KeyError as identifier:
        pass
