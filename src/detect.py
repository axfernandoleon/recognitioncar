from src.utils.vehicles import Car
from src.utils.roi import get_roi_frame, draw_roi
import cv2
import numpy as np
import time
import os
import ast
from datetime import datetime


def detect(input_video, droi, line_detect, save_video):

    cnt_up = 0
    cnt_down = 0

    # cap = cv2.VideoCapture("Freewa.mp4")
    cap = cv2.VideoCapture()
    cap.open(input_video)

    if save_video:
        # Generate name output
        uniq_filename = datetime.utcnow().strftime('%Y-%m-%d %H_%M_%S.%f')[:-3]
        # Save output recognition
        output = cv2.VideoWriter('output/'+uniq_filename+'.avi',
                                 cv2.VideoWriter_fourcc(*'XVID'), 20.0, (640, 480))

    w = cap.get(3)
    h = cap.get(4)
    frameArea = h*w
    areaTH = frameArea/400

    # Lines
    # line_up = int(2*(h/5))
    line_up = 0
    line_down = 220
    # Limmit
    # up_limit = int(1*(h/5))
    up_limit = 0
    down_limit = 266

    # print("Red line y:", str(line_up))
    # print("Blue line y:", str(line_down))

    line_down_color = (255, 0, 0)
    line_up_color = (255, 0, 255)
    # Line down

    pt1 = line_detect[0]
    pt2 = line_detect[1]
    pts_L1 = np.array([pt1, pt2], np.int32)
    pts_L1 = pts_L1.reshape((-1, 1, 2))
    # Line up
    pt3 = [147, line_up]
    pt4 = [422, line_up]
    pts_L2 = np.array([pt3, pt4], np.int32)
    pts_L2 = pts_L2.reshape((-1, 1, 2))

    # Limit up
    pt5 = [184, up_limit]
    pt6 = [381, up_limit]
    pts_L3 = np.array([pt5, pt6], np.int32)
    pts_L3 = pts_L3.reshape((-1, 1, 2))

    # Limit down
    pt7 = line_detect[2]
    pt8 = line_detect[3]
    pts_L4 = np.array([pt7, pt8], np.int32)
    pts_L4 = pts_L4.reshape((-1, 1, 2))

    # Background Subtractor
    fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=True)

    # Kernals
    kernalOp = np.ones((3, 3), np.uint8)
    kernalOp2 = np.ones((5, 5), np.uint8)
    kernalCl = np.ones((11, 11), np.uint)

    font = cv2.FONT_HERSHEY_SIMPLEX
    cars = []
    max_p_age = 5
    pid = 1

    while(cap.isOpened()):
        ret, frame = cap.read()

        # Area de interes
        droi_frame = get_roi_frame(frame, droi)
        # droi_frame = cv2.cvtColor(droi_frame, cv2.COLOR_BGR2HSV)
        cv2.imshow('droi_frame', droi_frame)
        for i in cars:
            i.age_one()
        fgmask = fgbg.apply(droi_frame)
        fgmask2 = fgbg.apply(frame)

        if ret == True:

            # mask = cv2.equalizeHist(fgmask)
            # Binarization

            ret, imBin = cv2.threshold(fgmask, 150, 255, cv2.THRESH_BINARY)
            ret, imBin2 = cv2.threshold(fgmask2, 200, 255, cv2.THRESH_BINARY)

            # OPening i.e First Erode the dilate
            mask = cv2.morphologyEx(imBin, cv2.MORPH_OPEN, kernalOp)
            mask2 = cv2.morphologyEx(imBin2, cv2.MORPH_CLOSE, kernalOp)

            # Closing i.e First Dilate then Erode
            # mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernalCl)
            # mask2 = cv2.morphologyEx(mask2, cv2.MORPH_CLOSE, kernalCl)

            cv2.imshow('mask', mask)
            # Find Contours
            countours0, hierarchy = cv2.findContours(
                mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for cnt in countours0:
                area = cv2.contourArea(cnt)
                # print(area)
                if area > areaTH:
                    ####Tracking######
                    m = cv2.moments(cnt)
                    cx = int(m['m10']/m['m00'])
                    cy = int(m['m01']/m['m00'])
                    x, y, w, h = cv2.boundingRect(cnt)

                    new = True
                    if cy in range(up_limit, down_limit):
                        for i in cars:
                            if abs(x - i.getX()) <= w and abs(y - i.getY()) <= h:
                                new = False
                                i.updateCoords(cx, cy)

                                if i.going_UP(line_down, line_up) == True:
                                    cnt_up += 1
                                    print("ID:", i.getId(),
                                          'crossed going up at', time.strftime("%c"))
                                elif i.going_DOWN(line_down, line_up) == True:
                                    cnt_down += 1
                                    print("ID:", i.getId(),
                                          'crossed going up at', time.strftime("%c"))
                                break
                            if i.getState() == '1':
                                if i.getDir() == 'down' and i.getY() > down_limit:
                                    i.setDone()
                                elif i.getDir() == 'up' and i.getY() < up_limit:
                                    i.setDone()
                            if i.timedOut():
                                index = cars.index(i)
                                cars.pop(index)
                                del i

                        if new == True:  # If nothing is detected,create new
                            p = Car(pid, cx, cy, max_p_age)
                            cars.append(p)
                            pid += 1

                    cv2.circle(frame, (cx, cy), 5, (0, 0, 255), -1)

                    img = cv2.rectangle(
                        frame, (x, y), (x+w, y+h), (0, 255, 0), 2)

            for i in cars:
                cv2.putText(frame, str(i.getId()), (i.getX(), i.getY()),
                            font, 0.3, i.getRGB(), 1, cv2.LINE_AA)

            # str_up = 'UP: '+str(cnt_up)
            str_down = 'Count: '+str(cnt_down)

            frame = cv2.polylines(
                frame, [pts_L1], False, line_down_color, thickness=2)
            # frame = cv2.polylines(
            #     frame, [pts_L2], False, line_up_color, thickness=2)
            # frame = cv2.polylines(
            #     frame, [pts_L3], False, (255, 255, 255), thickness=1)
            frame = cv2.polylines(
                frame, [pts_L4], False, (255, 255, 255), thickness=1)

            # cv2.putText(frame, str_up, (10, 40), font, 2,
            #             (255, 255, 255), 2, cv2.LINE_AA)
            # cv2.putText(frame, str_up, (10, 40), font,
            #             2, (0, 0, 255), 1, cv2.LINE_AA)
            # cv2.putText(frame, str_down, (10, 90), font,
            #             2, (255, 255, 255), 2, cv2.LINE_AA)
            frame = draw_roi(frame, droi)
            cv2.putText(frame, str_down, (467, 33), font,
                        1, (255, 0, 0), 2, cv2.LINE_AA)

            cv2.namedWindow("Frame", cv2.WINDOW_NORMAL)
            cv2.resizeWindow('Frame', 900, 600)
            cv2.imshow('Frame', frame)
            # cv2.moveWindow('Frame', 500, 150)
            if save_video:
                output.write(frame)

            if cv2.waitKey(1) & 0xff == ord('q'):
                break

        else:
            break
    cap.release()
    if save_video:
        output.release()
    cv2.destroyAllWindows()
